<?php

/* 
 * The homepage for all of the small projects
 */
?>

<link rel="stylesheet" href="css/main.css" type="text/css">

<div class="page-content">

    <h1>Welcome.</h1>
    <h2>To a collection of solutions to common programming conundrums.</h2>
    <h3>A Place of Experimentation and testing.</h3>

    <div class="main-box">
        
        <a href="fizz_buzz/main.php">
            <div class="experiment">
                Fizz Buzz
            </div>
        </a>
        
        <a href="guess_the_number/main.php">
            <div class="experiment">
                Guess The Number
            </div>
        </a>
        
    </div>

</div>






