<?php

/* 
 * Copyright (C) 2017 Andy Carr
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */
?>

<link rel="stylesheet" href="../css/main.css" type="text/css">
<link rel="stylesheet" href="../css/guess_the_number.css" type="text/css">


<?php 
    $random_number      = rand(0,10);
    $secret_number      = ( isset($_POST['secret_number'])  ? $_POST['secret_number']   : $random_number ); 
    $guesses            = ( isset($_POST['guesses'])        ? $_POST['guesses'] + 1     : 0 ); 
    $guess              = ( isset($_POST['guess'])          ? $_POST['guess']           : 0 ); 
    $try_again_message  = "Finding this one too hard? Try again here:";

    if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['guess-submit']))
    {
        $message = "You guessed: {$guess}.";

        if ($guess == $secret_number)
        {
            $result_message = "This is Correct!! Congratulations, this took you {$guesses} attempts.";
            $try_again_message = "Like to play again? Have another go here:";
        }
        elseif ($guess > $secret_number)
        {
            $result_message = "Your guess is too high. Try a lower number";
        }
        elseif ($guess < $secret_number)
        {
            $result_message = "Too low!! try again.";
        }
    }
?>



<div class="page-content">
    
    <div class="back">
        <a href="../index.php"><- Back</a>
    </div>

    <h1>Guess the Number</h1>
    <p>I am thinking of a number between 1 and 10. Can you guess what it is? How many guesses will it take you?</p>
    <p>Enter your guess here:</p>

    <div class="form-area">
        <form action="main.php" method="post">
            <div class="text-field-area">
                <input type="text" class="guess-the-age" name="guess" tabindex="0">
                <input type="hidden" name="secret_number" value="<?= $secret_number; ?>">
                <input type="hidden" name="guesses" value="<?= $guesses; ?>">
            </div>
            <div class="button-area">
                <input type="submit" class="submit-button" name="guess-submit" value="Guess">
            </div>
        </form>
    </div>
    
    <div class="feedback">
        <?= $message; ?><br />
        <?= $result_message; ?>
    </div>
    
    <h2><br /><?= $try_again_message; ?></h2>
    
    <div class="form-area">
        <form action="main.php" method="post">
            <div class="button-area">
                <input type="submit" class="submit-button" name="reset" value="Start a new Game">
            </div>
        </form>
    </div>

</div>


