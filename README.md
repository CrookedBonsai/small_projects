# Small Projects #

### What is this repository for? ###

This is a collection of puzzles and challenges i have worked on in PHP.

Each mini-project is a standalone entity, with no dependencies on any other project.

I have also been working with Unit Testing to test each sample of code.

### Repository Owner ###

* Andy Carr