<?php

/* 
 * A simple implimentation of the Common Fizz-Buzz conumndrum
 * 
 * Rules:
 * 
 * Output the numbers from 1 to 100
 * Output Fizz if the number is divisible by 3
 * Output Buzz if the number is divisible by 5
 * Output FizzBuzz if divisible by both
 */
namespace projects;
?>

<link rel="stylesheet" href="../css/main.css" type="text/css">
<link rel="stylesheet" href="../css/fizz_buzz.css" type="text/css">

<div class="page-content">
    
    <div class="back">
        <a href="../index.php"><- Back</a>
    </div>
    
    <h1>Fizz Buzz</h1>
    <p>A simple implimentation of the Common Fizz-Buzz conumndrum</p>
    <h3>Rules:</h3>
    <ul>
        <li>Output the numbers from 1 to 100</li>
        <li>Output <span class="fizz">Fizz</span> if the number is divisible by 3</li>
        <li>Output <span class="buzz">Buzz</span> if the number is divisible by 5</li>
        <li>Output <span class="fizz-buzz">FizzBuzz</span> if divisible by both</li>
    </ul>

    <p>
    <?php
    for($i=1; $i<=100; $i++)
    {
        // Assigning the appropriate output
        switch($i)
        {   
            case(is_integer($i/3) && is_integer($i/5)):
                $output =  '<span class="fizz-buzz">FizzBuzz</span>';
                break;
            case(is_integer($i/3)):
                $output =  '<span class="fizz">Fizz</span>';
                break;
            case(is_integer($i/5)):
                $output = '<span class="buzz">Buzz</span>';
                break;
            default:
                $output = "$i";
        }   

        // Simple formatting to provide comma delimitation and line breaks
        echo $output;
        if($i < 100)
        {
            echo ", ";
            if(is_integer($i / 25)) {
                echo "<br/>\n";
            }
        } 
    }
    ?>
    </p>
</div>